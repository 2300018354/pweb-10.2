<html lang="en">
<head>
   <title>Konversi nilai ke angka </title>
</head>
<body>
    <form action="" method="post" name="nilai">
        Masukkan Nilai: <input type="text" name="nilai"><br>
        <input type="submit" name="submit" value="Tampilkan">
    </form>
</body>
</html>


<?php
if(isset($_POST['submit'])){
    $nilai = $_POST['nilai'];
    
    if(is_numeric($nilai)){
        $nilai = floatval($nilai); 
        echo "Masukkan Nilai: $nilai<br>";

        if($nilai >= 80 && $nilai <= 100){
            echo "Nilai Anda $nilai, Anda Mendapat A";
        } elseif($nilai >= 76.25 && $nilai <= 79.99){
            echo "Nilai Anda $nilai, Anda Mendapat A-";
        } elseif($nilai >= 68.75 && $nilai <= 76.24){
            echo "Nilai Anda $nilai, Anda Mendapat B+";
        } elseif($nilai >= 65.00 && $nilai <= 68.74){
            echo "Nilai Anda $nilai, Anda Mendapat B";
        } elseif($nilai >= 62.50 && $nilai <= 64.99){
            echo "Nilai Anda $nilai, Anda Mendapat B-";
        } elseif($nilai >= 57.50 && $nilai <= 62.49){
            echo "Nilai Anda $nilai, Anda Mendapat C+";
        } elseif($nilai >= 55.00 && $nilai <= 57.49){
            echo "Nilai Anda $nilai, Anda Mendapat C";
        } elseif($nilai >= 51.25 && $nilai <= 54.99){
            echo "Nilai Anda $nilai, Anda Mendapat C-";
        } elseif($nilai >= 43.75 && $nilai <= 51.24){
            echo "Nilai Anda $nilai, Anda Mendapat D+";
        } elseif($nilai >= 40.00 && $nilai <= 43.74){
            echo "Nilai Anda $nilai, Anda Mendapat D";
        } elseif($nilai >= 0 && $nilai <= 39.99){
            echo "Nilai Anda $nilai, Anda Mendapat E";
        } else{
            echo "Nilai Tidak Valid";
        }
    } else {
        echo "Nilai harus berupa angka.";
    }
}
?>
